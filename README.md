# Floating Boat and Dynamic Water Simulation

Study from YouTube tutorial:
https://www.youtube.com/watch?v=eL_zHQEju8s

The boat is modelised by a rectancle GameObject for now, and the buoyancy forces are applied using floaters that are invisible GameObject attached to the boat.
Wave generation is modelised using a mesh grid.

To manage Wave generation, select the Water GameObject in the scene and modify the WaveManager script values.
To manage the boat buoyancy, select the Rectangle GameObject in the scene and modify the Floater script values of the GameObject and his child.

To be continued with:
https://catlikecoding.com/unity/tutorials/flow/waves/
And
https://www.youtube.com/watch?v=kGEqaX4Y4bQ
For better wave generation

And manipulate waves mesh vertices on GPU instead of CPU for way better performance.
with (again) :
https://catlikecoding.com/unity/tutorials/flow/waves/


Demo gif from the project:

![image alternative text](/Demo.gif)
