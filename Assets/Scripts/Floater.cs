using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//works with WaveManager script to make the object its attached on float on the water
public class Floater : MonoBehaviour
{
    // change rigidbody drag to move the object more slowly
    public Rigidbody rigidbody; 
    public float depthBeforeSubmerged = 1f;
    public float displacementAmount = 3f; // full buoyancy force factor ( in reality depends on the fluid we're in i guess)
    public int floatersCount = 1; //number of floaters attached to the parent object ( ie to the boat )

    //can also be >1
    public float waterDrag = 0.99f;
    public float waterAngularDrag = 0.5f;


    private void Awake()
    {
        // dont forget to desactivate the Use Gravity cause we already apply gravity in this script
        rigidbody.useGravity = false;
    }

    private void FixedUpdate()
    {
        // if we dont / by floatersCount we will apply gravity force floatersCount times to the boat
        rigidbody.AddForceAtPosition(Physics.gravity / floatersCount, transform.position, ForceMode.Acceleration);


        float waveHeight = WaveManager.instance.GetWaveHeight(transform.position.x);
        if (transform.position.y < waveHeight)//object considered unnderwater: apply buoyancy force
        {
            //buoyancy force applied to the object: value in [0,1] stating how immerged we are * a scaling factor
            float displacementMultiplier = Mathf.Clamp01((waveHeight-transform.position.y) / depthBeforeSubmerged) * displacementAmount;
            // apparently the buoyancy force is relative to the gravity force ( the more you know )
            // and Acceleration ForceMode ignore the mass of the objet which is the case for buoyancy force
            //and AddForceAtPosition and not AddForce to make the boat sway ( when attached to multiplefloaters )
            rigidbody.AddForceAtPosition(new Vector3(0f,Mathf.Abs(Physics.gravity.y)*displacementMultiplier,0f),transform.position,ForceMode.Acceleration);

            //handle drags to make the object more stable, reduce move/rotate movements
            rigidbody.AddForce(displacementMultiplier * -rigidbody.velocity * waterDrag *Time.deltaTime, ForceMode.VelocityChange);
            rigidbody.AddTorque(displacementMultiplier * -rigidbody.angularVelocity * waterAngularDrag * Time.deltaTime, ForceMode.VelocityChange);

        }

    }
}
