using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaveManager : MonoBehaviour
{
    public static WaveManager instance;
    public float amplitude = 1f;
    public float length = 2f;
    public float speed = 1f;
    public float offset = 0f;

    private void Awake()
    {
        if (instance == null)
            instance = this;
        else
            Destroy(this);
    }
    private void Update()
    {
        offset += Time.deltaTime * speed;
    }
    public float GetWaveHeight( float _x)
    {
        //check wave maths section in https://www.youtube.com/watch?v=kGEqaX4Y4bQ
        return amplitude * Mathf.Sin(_x/length + offset);
    }
}
