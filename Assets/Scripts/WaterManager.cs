using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//impose that whatever GameObject this script is attached on has a MeshFilter & MeshRender component
[RequireComponent(typeof(MeshFilter))]
[RequireComponent(typeof(MeshRenderer))]

public class WaterManager : MonoBehaviour
{
    private MeshFilter meshFilter;
    private MeshRenderer meshRenderer;

    private void Awake()
    {
        meshFilter = GetComponent<MeshFilter>();
    }
    private void Update()
    {
        Vector3[] vertices = meshFilter.mesh.vertices;
        for( int i = 0; i < vertices.Length; i++)
        {
            //adjust water height by modifying y value of
            //vertices on point position x ( global not relative to parent thats why we had transform.position.x)
            vertices[i].y = WaveManager.instance.GetWaveHeight(transform.position.x + vertices[i].x);
        }
        meshFilter.mesh.vertices = vertices;
        //we have to make sure the normals are correct since we changed the vertices pos
        meshFilter.mesh.RecalculateNormals();
    }
}
